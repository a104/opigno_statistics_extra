<?php

namespace Drupal\opigno_statistics_extra\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\opigno_module\Entity\UserModuleStatus;
use Drupal\user\UserInterface;
use Drupal\Core\Url;
use Drupal\opigno_module\Entity\OpignoModule;
use Drupal\Core\Form\ConfirmFormHelper;
use Drupal\opigno_learning_path\Entity\LPStatus;

/**
 * Defines a confirmation form to clear statistics data for a user.
 */
class UserStatisticsConfirmResetForm extends ConfirmFormBase {

  /**
   * User Object.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Group Object.
   *
   * @var \Drupal\group\Entity\GroupInterface
   */
  protected $group;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = NULL, UserInterface $user = NULL) {
    $this->user = $user;
    $this->group = $group;

    // Make sure the group provided is a Learning Path.
    if ($this->group->getGroupType()->id() != 'learning_path') {
      $form = [
        '#type' => 'item',
        '#markup' => t("The group provided needs to be a Learning Path."),
      ];
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['cancel'] = ConfirmFormHelper::buildCancelLink($this, $this->getRequest());

      return $form;
    }

    $form = parent::buildForm($form, $form_state);

    // Because of the impact this action has, we are adding a second layer
    // of form validation before deleting data.
    $form['text_validation'] = [
      '#type' => 'textfield',
      '#title' => t('Extra Validation'),
      '#field_prefix' => $this->t('Please type in the following: <b>@word</b>', ['@word' => $this->t('I confirm')]),
      '#required' => TRUE,
      '#weight' => -99,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $text_validation = $form_state->getValue('text_validation');
    $text = t('I confirm');
    if (strtolower($text_validation) !== strtolower($text)) {
      $form_state->setErrorByName('text_validation',
        $this->t('Invalid text entry. Value does not match: <b>@word</b>', ['@word' => $this->t('I confirm')])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operations = [];

    $operations[] = [
      [get_class($this), 'processBatch'],
      [
        'user_lp_status',
        [
          'user' => $this->user,
          'group' => $this->group,
        ],
      ],
    ];

    $operations[] = [
      [get_class($this), 'processBatch'],
      [
        'achievements',
        [
          'user' => $this->user,
          'group' => $this->group,
        ],
      ],
    ];

    $modules = opigno_learning_path_get_modules_ids_by_group($this->group);
    foreach ($modules as $module_id) {
      $operations[] = [
        [get_class($this), 'processBatch'],
        [
          'module',
          [
            'user' => $this->user,
            'group' => $this->group,
            'module_id' => $module_id,
          ],
        ],
      ];
    }

    $form_state->setRedirect('opigno_statistics.training', [
      'group' => $this->group->id(),
    ]);

    $batch = array(
      'title' => t('Deleting all statistics data for %training of %username.', [
        '%username' => $this->user->getDisplayName(),
        '%training' => $this->group->label(),
      ]),
      'operations' => $operations,
      'finished' => [$this, 'finishBatch'],
    );
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "user_statistics_confirm_reset_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('opigno_statistics.training', [
      'group' => $this->group->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Do you want to reset all statistics for %training of %username?', [
      '%username' => $this->user->getDisplayName(),
      '%training' => $this->group->label(),
    ]);
  }

  /**
   * Processes the batch and delete all the data.
   *
   * @param string $type
   *   The type of element to delete.
   * @param array $params
   *   An array with multiple objects.
   * @param array $context
   *   The batch context information.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processBatch($type, array $params, array &$context) {
    $msgs = [];
    $database = \Drupal::database();
    $entity_type_manager = \Drupal::entityTypeManager();
    $user = $params['user'];
    $user_id = $user->id();
    $group = $params['group'];
    $group_id = $group->id();

    if (empty($context['sandbox'])) {
      $context['results']['codes'] = [];
      $context['results']['group'] = $group;
      $context['results']['user'] = $user;
    }

    switch ($type) {
      case 'user_lp_status':
        $context['message'] = t('Deleting User Learning Path Status data for user @username', [
          '@username' => $user->getDisplayName(),
        ]);

        // Delete all user_lp_status.
        $storage_handler = $entity_type_manager->getStorage('user_lp_status');
        $entities = $storage_handler->loadByProperties([
          'uid' => $user_id,
          'gid' => $group_id,
        ]);
        $storage_handler->delete($entities);
        $msgs[] = t('Deleted all user leaning path status entities.');

        // Delete all user_lp_status_expire.
        LPStatus::removeCertificateExpiration($group_id, $user_id);
        $msgs[] = t('Deleted all Certificate Expiration.');

        $database
          ->delete('opigno_learning_path_step_achievements')
          ->condition('uid', $user_id)
          ->condition('gid', $group_id)
          ->execute();
        $msgs[] = t('Deleted Step Achievements');

        $database
          ->delete('opigno_learning_path_achievements')
          ->condition('uid', $user_id)
          ->condition('gid', $group_id)
          ->execute();
        $msgs[] = t('Deleted Achievements');
        break;

      case 'achievements':
        $context['message'] = t('Deleting achievements data for user @username', [
          '@username' => $user->getDisplayName(),
        ]);

        $database
          ->delete('opigno_learning_path_step_achievements')
          ->condition('uid', $user_id)
          ->condition('gid', $group_id)
          ->execute();
        $msgs[] = t('Deleted Step Achievements');

        $database
          ->delete('opigno_learning_path_achievements')
          ->condition('uid', $user_id)
          ->condition('gid', $group_id)
          ->execute();
        $msgs[] = t('Deleted Achievements');
        break;

      case 'module':
        $module_id = $params['module_id'];
        $module = OpignoModule::load($module_id);

        $context['message'] = t('Deleting module data for user @username', [
          '@username' => $user->getDisplayName(),
        ]);

        // If no module was found
        if (!$module) {
          $msgs[] = t('Module not found: @module_id', ['@module_id' => $module_id]);
          break;
        }
        elseif (!$module instanceof OpignoModule) {
          $msgs[] = t('Invalid module: @module_id', ['@module_id' => $module_id]);
          break;
        }

        $user_module_statuses = \Drupal::entityTypeManager()->getStorage('user_module_status')->loadByProperties(
          [
            'learning_path' => $group_id,
            'user_id' => $user_id,
            'module' => $module_id,
          ]
        );

        foreach ($user_module_statuses as $status) {
          if (!$status instanceof UserModuleStatus) {
            continue;
          }

          /** @var \Drupal\opigno_module\Entity\UserModuleStatusInterface $status */
          $answers = $status->getAnswers();
          foreach ($answers as $answer) {
            /** @var \Drupal\opigno_module\Entity\OpignoAnswerInterface $answer */
            $answer->delete();
            $msgs[] = t('Deleted answer ID: @id', ['@id' => $answer->id()]);
          }

          $status->delete();
          $msgs[] = t('Deleted user module status ID: @id', ['@id' => $status->id()]);
        }
        break;
    }

    $context['results']['msgs'] = $msgs;
  }

  /**
   * Batch finished callback: display batch statistics.
   *
   * @param bool $success
   *   Indicates whether the batch has completed successfully.
   * @param mixed[] $results
   *   The array of results gathered by the batch processing.
   * @param string[] $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function finishBatch($success, array $results, array $operations) {
    if ($success) {
      // Create new unfinished user module attempt on the first module
      // of training to disable the training resume.
      $group = $results['group'];
      $user = $results['user'];
      $modules = $group->getContentEntities('opigno_module_group');
      $module = reset($modules);
      if (!empty($module)) {
        $attempt = UserModuleStatus::create([
          'learning_path' => $group->id(),
        ]);
        $attempt->setModule($module);
        $attempt->setFinished(0);
        $attempt->setOwner($user);
        $attempt->save();
      }

      \Drupal::messenger()->addMessage(t('All statistics for %training of %username has been deleted.', [
        '%username' => $user->getDisplayName(),
        '%training' => $group->label(),
      ]));
    }
    else {
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(t('An error occurred while processing @operation with arguments: @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}
